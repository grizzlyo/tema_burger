package com.sda.builder;


import java.util.Scanner;

public class AppRunner {
    public static void main(String[] args) {

        boolean runProgram = true;

        Burger cchickenBurger = new Burger.Builder().withKetchup(true).withpaine(true).withCarne(true).withBranza(true).build();
        Burger beefBurger = new Burger.Builder().withpaine(true).withCarne(true).withLegume(true).build();
        Burger veganBurger = new Burger.Builder().withMustar(true).withpaine(true).withLegume(true).build();

        Scanner scanner = new Scanner(System.in);
        int options = 0;

        while (runProgram) {
            Display.displayMessage(Const.DISPLAY_CHICKEN_BURGER_MESSAGE);
            Display.displayMessage(Const.DISPLAY_BEEF_BURGER_MESSAGE);
            Display.displayMessage(Const.DISPLAY_VEGAN_BURGER_MESSAGE);
            Display.displayMessage(Const.DISPLAY_EXIT_MESSAGE);

            options = scanner.nextInt();

            switch (options) {
                case 1:
                    Display.displayBurger(cchickenBurger);
                    break;
                case 2 :
                    Display.displayBurger(beefBurger);
                    break;
                case 3 :
                    Display.displayBurger(veganBurger);
                    break;
                case 4 :
                    runProgram =false;
                    break;
                    default:
                        throw new IllegalArgumentException("Acest burger nu exista in meniul nostru!");
            }
        }
    }
}
