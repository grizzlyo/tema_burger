package com.sda.builder;

public class Burger {

    private boolean mustar;
    private boolean ketchup;
    private boolean paine;
    private boolean carne;
    private boolean branza;
    private boolean legume;

    @Override
    public String toString() {
        return "Burger{" +
                "mustar=" + mustar +
                ", ketchup=" + ketchup +
                ", paine=" + paine +
                ", carne=" + carne +
                ", branza=" + branza +
                ", legume=" + legume +
                '}';
    }

    public static class Builder{

        private boolean mustar;
        private boolean ketchup;
        private boolean paine;
        private boolean carne;
        private boolean branza;
        private boolean legume;

        public Builder withMustar(boolean mustar) {
            this.mustar = mustar;
            return this;
        }

        public Builder withKetchup(boolean ketchup) {
            this.ketchup = ketchup;
            return this;
        }

        public Builder withpaine(boolean paine) {
            this.paine = paine;
            return this;
        }

        public Builder withCarne(boolean carne) {
            this.carne = carne;
            return this;
        }

        public Builder withBranza(boolean branza) {
            this.branza = branza;
            return this;
        }

        public Builder withLegume(boolean legume) {
            this.legume = legume;
            return this;
        }

        public Burger build(){
            Burger burger = new Burger();

            burger.mustar = this.mustar;
            burger.ketchup = this.ketchup;
            burger.paine = this.paine;
            burger.carne = this.carne;
            burger.branza = this.branza;
            burger.legume = this.legume;

            return burger;
        }
    }
}
