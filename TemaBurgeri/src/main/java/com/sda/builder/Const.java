package com.sda.builder;

public interface Const {
    String DISPLAY_CHICKEN_BURGER_MESSAGE = "1. Chiken Burger;";
    String DISPLAY_BEEF_BURGER_MESSAGE = "2. Beef Burger;";
    String DISPLAY_VEGAN_BURGER_MESSAGE = "3. Vegan Burger;";
    String DISPLAY_EXIT_MESSAGE = "4. Exit;";
}
