package com.sda.builder;

public class Display {

    public static void displayMessage(String message){
        System.out.println(message);
    }

    public static void displayBurger(Burger burger){
        System.out.println(burger);
    }
}
